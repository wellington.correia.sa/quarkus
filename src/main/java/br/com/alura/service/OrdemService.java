package br.com.alura.service;

import br.com.alura.model.Ordem;
import br.com.alura.model.Usuario;
import br.com.alura.repository.OrdemRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.SecurityContext;
import java.time.LocalDate;
import java.util.Optional;

@ApplicationScoped
public class OrdemService {

    public void inserir(SecurityContext securityContext, Ordem ordem) {
        Optional<Usuario> usuarioOptional = Usuario.findByIdOptional(ordem.getUserId());
        Usuario usuario = usuarioOptional.orElseThrow();
        if(!usuario.getUserName().equals(securityContext.getUserPrincipal().getName())){
            throw new RuntimeException("O usuário logado é diferente do userID");
        }
        ordem.setData(LocalDate.now());
        ordem.setStatus("ENVIADA");
        Ordem.persist(ordem);
    }
}
