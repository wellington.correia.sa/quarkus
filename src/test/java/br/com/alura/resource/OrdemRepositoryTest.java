package br.com.alura.resource;

import br.com.alura.model.Ordem;
import br.com.alura.repository.OrdemRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Or;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@QuarkusTest
public class OrdemRepositoryTest {

    @InjectMock
    OrdemRepository ordemRepository;

    @Test
    public void testarSeListAllRetornaOrdensCorretas(){
        Ordem ordem1 = new Ordem();
        ordem1.setId(25l);
        ordem1.setStatus("APROVED");

        Ordem ordem2 = new Ordem();
        ordem2.setId(25l);
        ordem2.setStatus("APROVED");

        List<Ordem> ordens = new ArrayList<>();
        ordens.add(ordem1);
        ordens.add(ordem2);

        Mockito.when(ordemRepository.listAll()).thenReturn(ordens);

        Assertions.assertSame(ordem2, ordemRepository.listAll().get(1));
    }
}
